# Coin Op Gamers of Chattanooga: Code of Conduct

TL;DR: Be an adult. Be nice. We're all friends here.

Version: 4. Last updated: 2023-10-27.

---

Coin Op Gamers of Chattanooga does not allow abusive behavior at our
tournaments, League Nights, or casual play sessions. This includes, but is not
limited to:

1. taunting other players or teams (e.g. taunting while playing, taunting as a
   spectator or cheering against a team or opponent directly in front of you),
2. singling out other players,
3. talking to the other team or opponent during play in an aggressive or
   harassing manner,
4. excessive celebrations (e.g., loudly celebrating a win over a new or poorly
   matched team or opponent),
5. excessive cursing about characters in the abstract (e.g., calling a game
   character any insult or slur, etc), and
6. intimidating players or teams off the cabinet or on
7. unwanted sexual advances

If players or teams are behaving in any manners above, a Tournament Organizer,
as defined and chosen by the rules of the respective tournament, League Night,
or casual play, henceforth referred to as "TO", has the ability to rule on the
players or teams in the following way:

After a first offense, the TO has the authority to issue a verbal warning
including an explanation of the rules and what subsequent actions could result
in. If the player commits a second offense, however, whether similar or unique
to the original, it is grounds for the TO to force that player or player's team
to lose their next series or their current one if they are actively playing. On
the third offense, the TO has the power to remove that player or player's team
from the tournament entirely, including suggesting removal from the tournament
grounds. Ultimately, the bartender is free to decide whether a player should be
removed from the premises, even if a TO does not suggest doing so.

## Wait, who is a "TO" or "organizer" or "admin", actually?

Generally, organized events will have a core person or group of people running
the event. In those cases, we consider those people the TO of that event. Their
focus is to make sure the event runs well and take responsibility where
necessary, which may include bringing in outside help for decision making.

However, as we are largely an autonomous self-organized group with diverse needs
and interests, this is not meant to be an exclusive group. As a member of our
community, you have just as much rights as the members making sure events run
smoothly and are encouraged to participate in decision making and the direction
of the group. We want to encourage full democratic participation from all
members. You are a member if you are here.

## Anti-Harassment Policy

Harassment and hostility will not be tolerated in our tournaments, League Nights,
or casual play sessions. We strive to create an inclusive, accepting environment
and do not allow harassment of any kind. This includes, but is not limited to:

- Ableism
- Ageism
- Body Shaming
- Bullying
- Disrespectful Behavior
- Homophobia
- Racism
- Sexism
- Transphobia
- Deliberate intimidation
- Inappropriate photography or recording
- Inappropriate electronic communication
- Inappropriate physical contact and unwanted sexual attention
- Racial slurs
- Stalking
- Theft
- Other inappropriate behavior

Additionally, harassment transpiring in any event outside of locations which
host us in Chattanooga, regardless of whether or not it is held by a league or
tournament which utilizes this code of conduct or their own code of conduct, may
be subject to the same repercussions discussed below at league officials'
discretion. That is, this includes other places in Chattanooga, and other
cities.

Any harassment, expressed threats, or violent actions may result in immediate
ejection from play and a permanent ban from future events and any casual play.
Actions will also be reported to the venue and may be subject to discipline per
the venue's discretion.

Players are encouraged to report this behavior whether or not they were involved
in the exchange. The anonymity of the person(s) reporting issues will be
maintained at all times. Players are also encouraged to point out any time a TO
violates this code of conduct in any way.

We take accusations outside of play very seriously.

## COVID-19 Policy

Players should not attend activities, whether organized events or casual play,
if they have recently been exposed to COVID-19 or experience symptoms of
COVID-19. If you begin to feel symptoms during such activities, it is advised to
leave immediately.

An exposure may occur from many sources. A direct exposure is when you have
spent any time with a person known to have COVID, regardless whether that person
is contagious or positive at the time (this includes asymptomatic people), or whether they
were masked or not. An indirect exposure is when you have attended an event that
someone has notified they've tested positive for COVID afterwards (keep in mind
that they may have caught it from a person you _did have_ direct exposure to). An
assumed exposure is when you have attended a large indoor event (> ~30 people)
and when COVID detection is rising nationally or the locations visited (> ~300
average virus copies / mL of sewage, see also the resources below).

The following guidelines should be followed, with at minimum 1 negative test the
day you plan to attend:

- direct exposure: no attendance for 10 days, testing on the 6th day after last
  exposure (following CDC guidelines for exposure)
- indirect exposure: masked attendance or negative test for 10 days (new test
  each day attended)
- assumed exposure: masked attendance or negative test for 10 days (new test
  each day attended)

In summary, players that have been recently exposed in any fashion will be
required to have a negative test on the day of returning to any activities
locally, whether that is organized or casual play. We encourage (and prefer)
anyone with a recent exposure to wear a mask, regardless of recent test results.
If you are not sure, it is safest to stay home.

If you test positive and have attended anything recently (within 10 days),
please tell the group as soon as possible. If you are uncomfortable disclosing
this info publicly, please talk to any of the relevant organizers or persons you
trust and we will notify those necessary without disclosing the source.
**Knowingly exposing people while positive, even while masked, will result in an
immediate non-vote ban from all events and casual play of all games and
locations.**

Please do not publicly disclose information about someone who does not want
people to know. If you know someone who is directly violating this code of
conduct, please report this incident privately to any of the relevant
organizers.

Please do not mess around with this. We all have people in the community and
friends and family that are at high risk or just simply cannot afford to take
time off work. You may not feel sick, but limiting further exposure should be a
higher priority than playing a game. If you need masks or tests, just ask the
community -- we are here to support you, also. This has affected all of us, and
will continue to do so.

### COVID-19 Resources

- [Hamilton County metrics](<https://health.hamiltontn.org/en-us/allservices/communicablediseases/coronavirus(covid-19).aspx>)
- [Hamilton County Wastewater monitoring](https://data.wastewaterscan.org/tracker?charts=CiEQACABSABSBjU5ZWM5NFoGTiBHZW5leHiKAQYwNTFkNTI%3D&selectedChartId=051d52)
- [Biobot wastewater monitoring (historical only)](https://biobot.io/data/covid-19#county-47065.0)
- [CDC guidelines and exposure quiz](https://www.cdc.gov/coronavirus/2019-ncov/your-health/if-you-were-exposed.html)

## Additional notes

Accidents do happen and some habits are difficult to break, hence enforcement
will occur at the TO and community's discretion. That is, if the hype feels
positive and not as ill will, especially between established players, no action
may be taken unless concerns are raised by a player. However, please keep in
mind that what is a joke to you may not be a joke to another. This includes
certain memes, which may actually be problematic in a racist, sexist, etc way
you may not yet realize.

Players should also be aware that they are representing our scene whether it is
here in Chattanooga or in other arcade and pinball communities. The same abusive
behavior outlined above will not be tolerated while representing us within the
arcade community and ecosystem. This includes out of town tournaments or events
as well as online arcade communities (Facebook, YouTube, Twitch, etc).
Repercussions of abusive behavior may range anywhere from a warning to expulsion
from our events and host locations, depending on the severity of the action(s)
at the discretion of the community and the host.

## Acknowledgements

Many thanks to Woody Stanfield and the rest of the Killer Queen Chicago scene for
writing and providing the original Code of Conduct, as well as the Killer Queen
Charlotte scene for their amendments.
